import { useState } from 'react'
import './App.css'

function App() {

  const [cartcount,setcartcount] = useState(0)
  
function tocart(){
   setcartcount(cartcount + 1)
}

  return (
    <>
      <>
      <meta charSet="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <link
    rel="stylesheet"
    href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0"
  />
  
  <header>

    <div id="container" className="headers">
      <div className="headerleft">
        <span id="menubutton" className="material-symbols-outlined">
          menu
        </span>
        <div className="headerleftlg">
          <a id="left" href="#">
            Stores
          </a>
          <a id="left" href="#">
            Help &amp; Support
          </a>
        </div>
      </div>
      <div className="headercentre">
        <h3>SHOPPERS SPOT</h3>
      </div>
      <div className="headerright">
        <input
          id="search"
          className="search"
          type="text"
          placeholder="Search Products & Brands"
        />
        <span className="material-symbols-outlined">search</span>
        <span className="material-symbols-outlined">favorite</span>
        
        <div className='cartbutton'>
        <span className="material-symbols-outlined">shopping_bag </span>
        <span id='cartcount'>{cartcount}</span>
        </div>
       
        <span id="user" className="material-symbols-outlined">
          account_circle
        </span>
      </div>
    </div>
    <div className="headerbottom">
      <a id="bottomtags" href="#">
        MEN
      </a>
      <a id="bottomtags" href="#">
        WOMEN
      </a>
      <a id="bottomtags" href="#">
        BEAUTY
      </a>
      <a id="bottomtags" href="#">
        WATCHES
      </a>
      <a id="bottomtags" href="#">
        KIDS
      </a>
      <a id="bottomtags" href="#">
        HOMESTOP
      </a>
      <a id="bottomtags" href="#">
        GIFTS
      </a>
      <a id="bottomtags" href="#">
        BRANDS
      </a>
    </div>
  </header>
  <main>
    <div className="mainpicture">
      <img id="mainimage" src="./images/main.avif" alt="mainimage" />
      <h1 id="quote">The best things in life are purchased</h1>
      <button onClick={tocart} className="btn">ADD TO CART</button>
    </div>
    <img
      id="mainimage"
      src="./images/authenticproducts.webp"
      alt="authentic product"
    />
    <span id="brandleft">
      FEATURED <span id="brandright">BRANDS</span>
    </span>
    <div id="containerbrand">
      <img id="puma" src="./images/Puma.webp" alt="puma" />
      <img id="adidas" src="./images/adidas.webp" alt="adidas" />
      <img src="./images/timex.webp" alt="timex" />
    </div>
  </main>
  <footer>
    <div className="footer">
      <div className="footerfirst">
        <h5>SHOPPERS SPOT</h5>
        <a href="#">About us</a>
        <a href="#">Privacy policy</a>
      </div>
      <span>
        © 2024 Shoppers Spot Ltd. All rights reserved
        <br />
        <br />
        Terms of Use | Cookie &amp; Privacy Policy
      </span>
    </div>
  </footer>
</>

    </>
  )
}

export default App
